/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    editor,
    getHelpTopic,
    pgettext,
    tripetto,
} from "@tripetto/builder";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    kind: "headless",
    icon: ICON,
    get label() {
        return pgettext("block:error", "Raise error");
    },
})
export class Error extends NodeBlock {
    @editor
    defineEditor(): void {
        const helpTopic = getHelpTopic("block:error");

        this.editor.form({
            title: pgettext("block:error", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:error",
                        "Raises an error, prevents completion of the form and displays an optional message to the user%1.",
                        helpTopic
                            ? ` ([${pgettext(
                                  "block:error",
                                  "learn more"
                              )}](${helpTopic}))`
                            : ""
                    )
                ).markdown(),
            ],
        });

        this.editor.name(false, true, pgettext("block:error", "Error message"));
        this.editor.description();
        this.editor.groups.options();
        this.editor.visibility();
    }
}
