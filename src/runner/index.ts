import { NodeBlock, validator } from "@tripetto/runner";

export abstract class Error extends NodeBlock {
    @validator
    stopHere(): boolean {
        return false;
    }
}
